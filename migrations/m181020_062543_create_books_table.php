<?php

use yii\db\Migration;

/**
 * Handles the creation of table `books`.
 */
class m181020_062543_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('books', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer(),
            'name' => $this->string(64),
            'year' => $this->integer()->null(),
            'genre' => $this->integer()->null(),
            'imageName' =>  $this->string(64),
            'pagesCount' => $this->integer()
        ]);
        $this->addForeignKey('book_author_id', 'books', 'author_id', \app\models\Author::tableName(), 'id', 'CASCADE', 'NO ACTION');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('books');
    }
}
