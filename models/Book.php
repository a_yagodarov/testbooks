<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "books".
 *
 * @property int $id
 * @property string $name
 * @property int $year
 * @property int $genre
 * @property string $imageName
 * @property int $pagesCount
 */
class Book extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['imageFile'], 'file', 'extensions' => 'jpg, jpeg, png'],
//            [['imageFile'], 'required'],
            [['year', 'genre', 'pagesCount', 'name'], 'required'],
            [['year', 'genre', 'pagesCount', 'author_id'], 'integer'],
            [['name', 'imageName'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'author_id' => 'Автор',
            'name' => 'Название',
            'year' => 'Год',
            'genre' => 'Жанр',
            'imageName' => 'Изображение',
            'imageFile' => 'Изображение',
            'file' => 'Фото',
            'pagesCount' => 'Кол-во страниц',
        ];
    }

    public function upload()
    {
        $path = Yii::getAlias('@app/web/files/images/');
        $filename = $this->getAttribute('id');
        $this->image->saveAs($path . $filename . '.' . $this->image->extension);
        $this->image = $filename . '.' . $this->image->extension;
        $this->getImagePath();
    }

    public static function getGenreList()
    {
        return [
            1 => 'Проза',
            2 => 'Роман',
            3 => 'Детектив',
            4 => 'Новелла'
        ];
    }
}
