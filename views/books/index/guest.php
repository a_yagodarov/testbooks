<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 20.10.2018
 * Time: 10:21
 */
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'year',
            [
                'attribute' => 'genre',
                'value' => function($model) {
                    return \app\models\Book::getGenreList()[$model->genre];
                },
                'filter'=>\app\models\Book::getGenreList()
            ],
            [
                'attribute' => 'imageName',
                'format' => 'raw',
                'value' => function($model)
                {
                    if ($model->imageName) {
                        $path = Yii::getAlias('/web/files/images/'.$model->imageName);
                    } else {
                        $path = Yii::getAlias('/web/files/images/empty.jpg');
                    }
                    return Html::img($path, [
                        'height' => '100px',
                        'width' => 'auto'
                    ]);
                }
            ],
            //'pagesCount',
        ],
    ]); ?>
</div>